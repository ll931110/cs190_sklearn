
# coding: utf-8

# In[1]:

"""
Logistic regression implemented on the CTR dataset
"""
from test_helper import Test
import requests
import tarfile

fileName = 'dac_sample.tar.gz'
url = 'http://labs.criteo.com/wp-content/uploads/2015/04/dac_sample.tar.gz'
with open(fileName, 'wb') as f:
    response = requests.get(url)
    if not response.ok:
        raise ValueError, "Unable to download the file. Check the link"
        
    for chunk in response.iter_content(chunk_size=1024):
        if chunk:
            f.write(chunk)
            
    print "Download file successfully"
    f.close()
    
tar = tarfile.open(fileName)
tar.extractall()
tar.close()


# In[2]:

textFile = 'dac_sample.txt'
f = open(textFile, 'rb')

raw_label = []
raw_feature = []
for line in f.readlines():
    line = line.replace('\t', ' ')
    line = line.rstrip('\n')
    
    elements = line.split(' ')
    raw_label.append(int(elements[0]))
    raw_feature.append([x for x in elements[1:]])


# In[4]:

Test.assertEquals(len(raw_label), 100000, "Incorrect number of data point")
Test.assertEquals(len(raw_feature[0]), 39, "Incorrect number of features")


# In[3]:

import numpy as np

label = np.array(raw_label)
feature = np.array(raw_feature)


# In[7]:

from sklearn.cross_validation import train_test_split

feature_train, feature_test, label_train, label_test =     train_test_split(feature, label, test_size = 0.2, random_state = 42)
    
Test.assertEquals(feature_train.shape[0], 80000, "Incorrect size of training set")
Test.assertEquals(feature_test.shape[0], 20000, "Incorrect size of testing set")


# In[36]:

def createOneHotEncoding(feature_train):
    """
    Generate a one-hot encoding from a set of features
    (Since there are string-like features inside the data set, the default API does not work)
    Argument:
        feature_train: a matrix of features
    Return:
        a dictionary of (index, feature) to int
    """
    mapping = {}
    counter = 0
    for point in feature_train:
        for i, c in enumerate(point):
            if (i, c) not in mapping:
                mapping[(i, c)] = counter
                counter += 1
    return mapping

OHE_train = createOneHotEncoding(feature_train)
# This number of keys depend on the random-generator, which should be fixed if we fix the key
Test.assertEquals(len(OHE_train.keys()), 233397, "Incorrect number of keys")


# In[37]:

from scipy.sparse import csr_matrix

def produceOHE(data):
    """
    Given the data, encode it using the OHE_encoding
    Missing values will be denoted as 0
    
    Argument:
        data: a matrix of features
    Return:
        a 2D array of encoded features using integers
    """
    
    rows = []
    cols = []
    values = []
    for r, point in enumerate(data):
        for c, feat in enumerate(point):
            if (c, feat) in OHE_train:
                rows.append(r)
                cols.append(OHE_train[(c, feat)])
                values.append(1)
    return csr_matrix( (values, (rows, cols)), shape=(data.shape[0], len(OHE_train.keys()) ) )

feature_OHE_train = produceOHE(feature_train)
feature_OHE_test = produceOHE(feature_test)


# In[38]:

from math import log

def computeLogLoss(p, y):
    """Calculates the value of log loss for a given probabilty and label.

    Note:
        log(0) is undefined, so when p is 0 we need to add a small value (epsilon) to it
        and when p is 1 we need to subtract a small value (epsilon) from it.

    Args:
        p (float): A probabilty between 0 and 1.
        y (int): A label.  Takes on the values 0 and 1.

    Returns:
        float: The log loss value.
    """
    epsilon = 10e-12
    if y == 0:
        error = 1 - p
    else:
        error = p
    if error == 0:
        error += epsilon
    if error < epsilon:
        print error
        raise ValueError
    return -log(error)

Test.assertTrue(np.allclose([computeLogLoss(.5, 1), computeLogLoss(.01, 0), computeLogLoss(.01, 1)],
                            [0.69314718056, 0.0100503358535, 4.60517018599]),
                'computeLogLoss is not correct')
Test.assertTrue(np.allclose([computeLogLoss(0, 1), computeLogLoss(1, 1), computeLogLoss(1, 0)],
                            [25.3284360229, 1.00000008275e-11, 25.3284360229]),
                'computeLogLoss needs to bound p away from 0 and 1 by epsilon')


# In[39]:

def datasetLogLoss(label, prediction):
    """
    Compute the average log loss 
    """
    errors = np.array([computeLogLoss(p, y) for p, y in zip(prediction, label)])
    return errors.sum()/errors.size


# In[40]:

from sklearn.linear_model import LogisticRegression

logit = LogisticRegression()
logit.fit(feature_OHE_train, label_train)


# In[44]:

print logit.predict_proba(feature_OHE_train[0])[0][1]


# In[45]:

# 1 should correspond to the probability of predicting 1
logitTrainPrediction = np.array([logit.predict_proba(X)[0][1] for X in feature_OHE_train])
logitTestPrediction = np.array([logit.predict_proba(X)[0][1] for X in feature_OHE_test])

print datasetLogLoss(feature_test, logitTrainPrediction)
print datasetLogLoss(label_test, logitTestPrediction)


# In[ ]:



