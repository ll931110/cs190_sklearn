# -*- coding: utf-8 -*-
# <nbformat>3.0</nbformat>

# <codecell>

from test_helper import Test
import os.path
import numpy as np

baseDir = os.path.join(os.getcwd(), '../data/linear')
inputFileName = 'YearPredictionMSD.txt'
fileName = os.path.join(baseDir, inputFileName)

# <codecell>

f = open(fileName, 'rb')
raw_feature = []
raw_label = []
for line in f.readlines():
    raw_line = line.split(',')
    raw_label.append(float(raw_line[0]))
    raw_feature.append([float(x) for x in raw_line[1:]])
feature = np.array(raw_feature)
label = np.array(raw_label)

Test.assertEquals(feature.shape[0], 515345, "Incorrect number of lines")
Test.assertEquals(feature.shape[1], 90, "Incorrect number of features")
Test.assertEquals(label.shape[0], 515345, "Incorrect number of target lines")

# <codecell>

minYear = label.min()
maxYear = label.max()
Test.assertEquals(minYear, 1922, "Incorrect min year")
Test.assertEquals(maxYear, 2011, "Incorrect max year")

# <codecell>

# Transform the years to be relative from the minYear
shifted_label = np.copy(label)
for i in xrange(label.shape[0]):
    shifted_label[i] -= minYear
Test.assertEquals(shifted_label.max(), 89, "Incorrect transformation")

# <codecell>

"""
For some reasons, the train_test_split in sklearn is malfunctioned
We will write our boilterplate split method
"""
import random

def train_test_split(feature, label, test_size, random_state=42):
    """
    Unofficial implementation of train_test_split
    Remove it when sklearn train_test_split works again
    """
    feature_size = feature.shape[0]
    train_index = []
    test_index = []
    for i in xrange(feature_size):
        if random.random() < test_size:
            test_index.append(i)
        else:
            train_index.append(i)
            
    feature_train, label_train = feature[train_index, :], label[train_index]
    feature_test, label_test = feature[test_index, :], label[test_index]
    return (feature_train, label_train, feature_test, label_test)

feature_train, label_train, feature_test, label_test = \
        train_test_split(feature, shifted_label, test_size=0.2)

# <codecell>

import math

def calcRMSE(label, prediction):
    """
    Argument:
        label (numpy array of float)
        prediction (numpy array of float)
    """
    diffs = np.subtract(label, prediction) ** 2
    return math.sqrt(diffs.mean())

sample_label = np.array([3, 1, 2])
sample_prediction = np.array([1, 2, 2])
Test.assertTrue(np.allclose(calcRMSE(sample_label, sample_prediction), 1.29099444874), \
                "Incorrect value for RMSE")

# <codecell>

def makePrediction(feature, f):
    """
    Create a prediction given feature and a prediction function
    Argument:
        feature: a numpy matrix of feature
        f: the prediction function given a single data point
    Return:
        a numpy array of prediction corresponding to feature
    """
    return f(feature)

# <codecell>

def baselinePrediction(feature):
    averageTrainYear = label_train.mean()
    results = np.empty(feature.shape[0])
    results.fill(averageTrainYear)
    return results

labelsPredictionTrain = makePrediction(feature_train, baselinePrediction)
rmseTrainBase = calcRMSE(label_train, labelsPredictionTrain)

labelsPredictionTest = makePrediction(feature_test, baselinePrediction)
rmseTestBase = calcRMSE(label_test, labelsPredictionTest)

print 'Baseline Train RMSE = {0:.3f}'.format(rmseTrainBase)
print 'Baseline Test RMSE = {0:.3f}'.format(rmseTestBase)

# <codecell>

from sklearn import linear_model

lr = linear_model.LinearRegression()
lr.fit(feature_train, label_train)

def linearRegressionFit(feature):    
    return np.array([lr.predict(X)[0] for X in feature])

# <codecell>

linearPredictionTrain = makePrediction(feature_train, linearRegressionFit)
rmseTrainLinear = calcRMSE(label_train, linearPredictionTrain)

linearPredictionTest = makePrediction(feature_test, linearRegressionFit)
rmseTestLinear = calcRMSE(label_test, linearPredictionTest)

print 'Linear Train RMSE = {0:.3f}'.format(rmseTrainLinear)
print 'Linear Test RMSE = {0:.3f}'.format(rmseTestLinear)

# <codecell>

"""
Bad design: we are forced to put ridge as global variable
"""
ridge = linear_model.Ridge(alpha=1.0)
ridge.fit(feature_train, label_train)

def ridgeRegressionFit(feature):
    return np.array([ridge.predict(X)[0] for X in feature])

# <codecell>

for alpha in [1e-1, 1e0, 1e1]:
    ridge = linear_model.Ridge(alpha=alpha)
    ridge.fit(feature_train, label_train)
    
    ridgePredictionTrain = makePrediction(feature_train, ridgeRegressionFit)
    rmseTrainRidge = calcRMSE(label_train, ridgePredictionTrain)
    
    ridgePredictionTest = makePrediction(feature_test, ridgeRegressionFit)
    rmseTestRidge = calcRMSE(label_test, ridgePredictionTest)
    
    print 'Ridge Train RMSE with alpha = %f is %f' % (alpha, rmseTrainRidge)
    print 'Ridge Test RMSE with alpha = %f is %f' % (alpha, rmseTestRidge)

# <codecell>

"""
BUG: given the data is 400 MB, twoway interaction will square the data size (160 GB), which is infeasible
Don't run this part
"""

def twoWayInteraction(point):
    """
    Create a new feature vector with all interactions, specifically, append [x * y] for all x, y in feature
    Argument:
        point: a 1D numpy array of features
    Return:
        a 1D numpy array of features, with interactions included
    """
    return np.append(point, [x * y for x in point for y in point])

feature_train_interact = np.apply_along_axis(twoWayInteraction, 1, feature_train)
feature_test_interact = np.apply_along_axis(twoWayInteraction, 1, feature_test)

Test.assertEquals(feature_train_interact.shape[0], feature_train.shape[0], "Incorrect interaction on train")
Test.assertEquals(feature_test_interact.shape[0], feature_test.shape[0], "Incorrect interaction on test")

Test.assertEquals(feature_train_interact.shape[1], \
                  feature_train.shape[1] * (feature_train.shape[1] + 1), "Incorrect feature count on test interaction")
Test.assertEquals(feature_test_interact.shape[1], \
                  feature_test.shape[1] * (feature_test.shape[1] + 1), "Incorrect feature count on test interaction")

# <codecell>



